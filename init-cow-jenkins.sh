#!/bin/bash

. .env

docker build -t lev-ecr:$TAG .

docker run -d -p 8081:8080 --name cowsay_testing --network cowsay_1_jenkins-net lev-ecr:$TAG


#!/bin/bash

. .env

ssh ubuntu@3.133.113.101 << EOF

    docker pull 644435390668.dkr.ecr.us-east-2.amazonaws.com/lev-ecr:$TAG

    docker run -d -p 8080:8080 --name cowsay-production 644435390668.dkr.ecr.us-east-2.amazonaws.com/lev-ecr:$TAG

EOF
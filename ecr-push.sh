#!/bin/bash

. .env

aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 644435390668.dkr.ecr.us-east-2.amazonaws.com

docker tag lev-ecr:$TAG 644435390668.dkr.ecr.us-east-2.amazonaws.com/lev-ecr:$TAG

docker push 644435390668.dkr.ecr.us-east-2.amazonaws.com/lev-ecr:$TAG


